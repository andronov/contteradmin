# -*- coding: utf-8 -*-
from slugify import slugify
from wand.image import Image
from wand.drawing import Drawing
from wand.color import Color

from django.core.management.base import BaseCommand, CommandError

from core.models import BaseLink
from core.utils import imager


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        print('START COMMAND')
        tester()
        print('END COMMAND')


def tester():
    for link in BaseLink.objects.all().order_by('-created')[0:100]:
        print('f', link.medium_img)
        json_img = {}
        name = slugify(link.title) + '_test.jpg'

        nm = 'C:\OpenServer2\OpenServer\domains\contter.dev\media\img\_' + name
        nwm = 'C:\OpenServer2\OpenServer\domains\contter.dev\media\img\_thumb_' + name
        with Image(filename=nm) as img:
            json_img["medium_img"] = {"height": img.height, "width": img.width}
            img.transform(resize='27x')
            img.format = 'jpeg'
            img.save(filename=nwm)

        thumbnail = '_thumb_' + name

        link.json_img = json_img
        link.thumbnail = thumbnail
        link.save()
    """
    nm = 'C:\Users\user\PycharmProjects\contest2\itest\_itest.jpg'
    #imager('itest.jpg', '22Nash records Hat Trick on overtime winner', 'desc')
    #img('itest.jpg', '22Nash records Hat Trick on overtime winner')
    new_name = 'thumb.jpg'
    nwm = 'C:\Users\user\PycharmProjects\contest2\itest\_'+str(new_name)
    with Image(filename=nm) as img:
        print('width =', img.width)
        print('height =', img.height)
        img.transform(resize='27x')
        img.format = 'jpeg'
        img.save(filename=nwm)
    """


def img(name, text):

    nm = 'C:\Users\user\PycharmProjects\contest2\itest\_'+str(name)

    i = 900
    while i <= 900:
        new_name = 'NEW_nhlF_'+str(i)+'_'+name
        nwm = 'C:\Users\user\PycharmProjects\contest2\itest\_'+str(new_name)
        with Image(filename=nm) as img:
            print('width =', img.width)
            print('height =', img.height)
            #img.crop(width=300, height=250, gravity='center')
            #img.transform(resize='600x')
            img.transform(resize=str(i)+'x')
            #img.level(0.2, 0.9, gamma=1.1)
            #img.format = 'jpeg'
            #img.save(filename='test2.jpg')
            with Drawing() as draw:
                # set draw.fill_color here? YES

                draw.fill_color = Color('#000000')
                draw.fill_opacity = 0.50
                sum = img.height - 110
                print(sum)
                draw.rectangle(left=0, top=sum, width=img.width, height=270)
                draw(img)
                #draw.font = 'C:\Users\user\PycharmProjects\contest\calibrib.otf'
                draw.font = 'C:\Users\user\PycharmProjects\contest\HelveticaNeueCyr-Medium.otf'
                draw.font_size = 16
                if i == 900:
                    draw.font_size = 42
                if i == 300:
                    draw.font_size = 12
                draw.fill_color = Color('#ffffff')
                #draw.color(Color('#000000'))
                draw.text(50, img.height - 60, text)
                draw(img)

                #draw.font = 'C:\Users\user\PycharmProjects\contest\calibrib.otf'
                draw.font = 'C:\Users\user\PycharmProjects\contest\HelveticaNeueCyr-Medium.otf'
                draw.font_size = 16
                if i == 900:
                    draw.font_size = 26
                if i == 300:
                    draw.font_size = 12
                draw.fill_color = Color('#ffffff')
                #draw.color(Color('#000000'))
                draw.text(50, img.height - 30, 'Rangers forward lifts team on his third goal of the night.')
                draw(img)

                #image.format = 'gif'
                img.format = 'jpeg'
                img.save(filename=nwm)
        i += 300