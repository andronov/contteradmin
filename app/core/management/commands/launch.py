# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

from core.engine import launch


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        print('START COMMAND')
        launch(2)
        print('END COMMAND')