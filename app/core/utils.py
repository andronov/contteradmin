import difflib
import threading
import textwrap
import re
from wand.image import Image
from wand.drawing import Drawing
from wand.color import Color

# Create your views here.
def imager(name, title, desc):

    try:
        print(title)
        print(desc)
    except:
        pass

    nm = 'C:\OpenServer2\OpenServer\domains\contter.dev\media\img\_'+str(name)

    i = 300
    while i <= 900:
        new_name = '_'+str(i)+'_'+name
        nwm = 'C:\OpenServer2\OpenServer\domains\contter.dev\media\img\_'+str(new_name)
        with Image(filename=nm) as img:
            print('width =', img.width)
            print('height =', img.height)
            #img.crop(width=300, height=250, gravity='center')
            #img.transform(resize='600x')
            img.transform(resize=str(i)+'x')
            #img.level(0.2, 0.9, gamma=1.1)
            #img.format = 'jpeg'
            #img.save(filename='test2.jpg')
            with Drawing() as draw:
                # set draw.fill_color here? YES

                #draw.fill_color = Color('#000000')
                #draw.fill_opacity = 0.20
                #sum = img.height - 70
                #print(sum)
                #draw.rectangle(left=0, top=2, width=img.width, height=img.height-2)
                #draw(img)

                """
                # new test

                #width, height = font.getsize(line)
                #draw.text(((w - width) / 2, y_text), line, font=font, fill=FOREGROUND)

                draw.font = 'C:\Users\user\PycharmProjects\contest\calibrib.otf'
                draw.font_size = 16
                font_top = img.height - 70#50
                font_left = 20
                if i == 900:
                    draw.font_size = 46
                    #font_top = 37
                    font_left = 300
                    w_title = 30
                    steper = 41
                if i == 300:
                    draw.font_size = 16
                    #font_top = 37
                    font_left = 80
                    w_title = 30
                    steper = 31
                if i == 600:
                    draw.font_size = 26
                    #font_top = 37
                    font_left = 145
                    w_title = 39
                    steper = 41

                title = textwrap.fill(title, initial_indent='', subsequent_indent='', width=w_title)
                step = len(re.findall("\n", title, re.IGNORECASE))


                draw.fill_color = Color('#ffffff')
                draw.font_weight = 600
                draw.text(font_left, font_top, title)
                draw(img)
                # new test
                """


                #width, height = font.getsize(line)
                #draw.text(((w - width) / 2, y_text), line, font=font, fill=FOREGROUND)

                draw.font = 'C:\Users\user\PycharmProjects\contest\calibrib.otf'
                draw.font_size = 16
                font_top = 50
                font_left = 20
                if i == 900:
                    draw.font_size = 46
                    font_top = 37
                    font_left = 22
                    w_title = 30
                    steper = 41
                if i == 300:
                    draw.font_size = 16
                    font_top = 37
                    font_left = 22
                    w_title = 30
                    steper = 31
                if i == 600:
                    draw.font_size = 26
                    font_top = 37
                    font_left = 22
                    w_title = 39
                    steper = 41

                title = textwrap.fill(title, initial_indent='', subsequent_indent='', width=w_title)
                step = len(re.findall("\n", title, re.IGNORECASE))


                draw.fill_color = Color('#ffffff')
                draw.font_weight = 600
                draw.text(font_left, font_top, title)
                draw(img)

                """end title"""


                #desc

                #width, height = font.getsize(line)
                #draw.text(((w - width) / 2, y_text), line, font=font, fill=FOREGROUND)

                draw.font = 'C:\Users\user\PycharmProjects\contest\calibrii0.otf'
                draw.font_size = 16

                font_left = 20
                if i == 900:
                    draw.font_size = 32
                    font_top = 80 + (font_top + (steper*step))#22
                    font_left = 20
                    w_desc = 53
                if i == 300:
                    draw.font_size = 12
                    font_top = 5 + (font_top + (steper*step))
                    font_left = 20
                    w_desc = 42
                if i == 600:
                    draw.font_size = 18
                    font_top = 22 + (font_top + (steper*step))
                    font_left = 20
                    w_desc = 55


                text = textwrap.fill(desc, initial_indent='', subsequent_indent='', width=w_desc)
                t, s = u'', 1
                for f in text.split('\n'):
                    if s <= 3:
                        if s == 3:
                            t += f + '...'
                        else:
                            t += f + '\n'
                    s += 1


                draw.fill_color = Color('#ffffff')
                #draw.font_weight = 100
                draw.font_style = 'italic'
                draw.text(font_left, font_top, t)
                draw(img)
                #end desc


                #image.format = 'gif'
                img.format = 'jpeg'
                img.save(filename=nwm)
        i += 300

class TimeoutError(RuntimeError):
    pass


class AsyncCall(object):
    def __init__(self, fnc, callback=None):
        self.Callable = fnc
        self.Callback = callback

    def __call__(self, *args, **kwargs):
        self.Thread = threading.Thread(target=self.run, name=self.Callable.__name__, args=args, kwargs=kwargs)
        self.Thread.start()
        return self

    def wait(self, timeout=None):
        self.Thread.join(timeout)
        if self.Thread.isAlive():
            raise TimeoutError()
        else:
            return self.Result

    def run(self, *args, **kwargs):
        self.Result = self.Callable(*args, **kwargs)
        if self.Callback:
            self.Callback(self.Result)


class AsyncMethod(object):
    def __init__(self, fnc, callback=None):
        self.Callable = fnc
        self.Callback = callback

    def __call__(self, *args, **kwargs):
        return AsyncCall(self.Callable, self.Callback)(*args, **kwargs)


def Async(fnc=None, callback=None):
    if fnc == None:
        def AddAsyncCallback(fnc):
            return AsyncMethod(fnc, callback)

        return AddAsyncCallback
    else:
        return AsyncMethod(fnc, callback)


def calculation_to_word(text1, word_filter, word_exclude):
    next = False
    txt1 = text1.replace(",", "").replace(".", "").lower().split(' ')
    if not word_filter:
        wf = []
    else:
        wf = word_filter.lower().split(', ')
    if not word_exclude:
        we = []
    else:
        we = word_exclude.lower().split(', ')


    if len(wf) > 0:
        sm_f = difflib.SequenceMatcher(None, txt1, wf)
        if sm_f.ratio() > 0:
            next = True
    else:
        if len(we) > 0:
            sm_e = difflib.SequenceMatcher(None, txt1, we)
            if sm_e.ratio() <= 0:
                next = True

    if len(wf) <= 0 and len(we) <= 0:
        next = True

    return next