# coding=utf-8
import datetime
import json
import urllib2
from wand.image import Image
from wand.drawing import Drawing
from wand.color import Color
from bs4 import BeautifulSoup
import feedparser
from slugify import slugify
from core.models import *
from core.utils import imager, Async, calculation_to_word
from django.db import connection
import psycopg2.extensions
"""
#
# LAUNCH PARSER
#
"""
def launch(id):
    base_site = BaseSite.objects.get(id=id)
    result = {'id': base_site.id, 'url': base_site.rss}
    print(result)
    parser(base_site)

"""
#
# RSS PARSER
#
"""
def parser(base_site):
    feed = feedparser.parse(base_site.rss)
    i = 1
    for entry in feed.entries:
        link = BaseLink.objects.filter(url=entry.link)
        print('link parser', link)
        if not link:
            base_link = func_entry(entry, base_site)
            if base_link:
                print('base_linkbase_link', base_link, base_link.id)
                update_user_link(base_link)
                """
                session.add(ed_user)
                session.commit()
                print('ed_usered_usered_usered_user', ed_user, ed_user.id)
                session.close()
                update_user_link(ed_user.id)
                """
        if i == 12:
            break
        i += 1

"""
#
# CREATE IMAGE
#
"""
request_headers = {
    "Accept-Language": "en-US,en;q=0.5",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Referer": "http://thewebsite.com",
    "Connection": "keep-alive"
}
def func_entry(entry, base_site):
        print('func_entry', entry['link'])
    #try:
        request = urllib2.Request(entry['link'], headers=request_headers)
        try:
            try:
                page = urllib2.urlopen(request)
                soup = BeautifulSoup(page)
            finally:
                page = urllib2.urlopen(entry['link'])
                soup = BeautifulSoup(page)
        except:
            print('error link')



        title = None
        try:
            try:
                title = soup.find("title").text
            finally:
                title = soup.find("meta", {"property": "og:title"})['content']
        except:
            pass

        description = None
        try:
            try:
                description = soup.find("meta", {"name": "description"})['content']
            finally:
                description = soup.find("meta", {"property": "og:description"})['content']
        except:
            pass

        keywords = None
        try:
            try:
                keywords = soup.find("meta", {"name": "keywords"})['content']
            finally:
                keywords = soup.find("meta", {"property": "og:keywords"})['content']
        except:
            pass



        img = None
        try:
            try:
                img = soup.find("meta", {"property": "og:image"})['content']
            finally:
                keywords = soup.find("meta", {"property": "og:keywords"})['content']
        except:
            pass
        print('imggggg', img)
        img = soup.find("meta", {"property": "og:image"})['content']
        name = slugify(title) + '_test.jpg'
        print('nameeee', name)
        nm = 'C:\OpenServer2\OpenServer\domains\contter.dev\media\img\_' + name
        f = open(nm, 'wb')
        print('ffffff', f)
        f.write(urllib2.urlopen(img).read())
        f.close()

        desc = ''
        if description:
            desc = description

        try:
            print(title)
            print(desc)
        except:
            pass
        #imager(name, title, desc)
        json_img = {}

        nwm = 'C:\OpenServer2\OpenServer\domains\contter.dev\media\img\_thumb_' + name
        with Image(filename=nm) as img:
            json_img["medium_img"] = {"height": img.height, "width": img.width}
            img.transform(resize='27x')
            img.format = 'jpeg'
            img.save(filename=nwm)

        thumbnail = '_thumb_' + name

        imager_300 = '_' + name
        imager_600 = '_' + name
        imager_900 = '_' + name

        html = str(soup)

        now = datetime.datetime.now()
        base_link = BaseLink.objects.create(site=base_site, title=title, description=description, keywords=keywords,
                                            is_active=True, url=entry.link, full_url=base_site.rss, small_img=imager_300,
                                            medium_img=imager_600, large_img=imager_900, tags='', created=now,
                                            html=html, thumbnail=thumbnail, json_img=json_img)
        #ed_user = BaseLink(site_id=id, title=title, description=description, keywords=keywords, is_active=True,
        #                   url=entry.link, full_url=url, small_img=imager_300,
        #                   medium_img=imager_600, large_img=imager_900, tags='', created=now)

        return base_link

"""
#
# UPDATE USER LINK, CREATE LINK FOR USER
#
"""
@Async
def update_user_link(base_link):
    print('base_linkbase_link', base_link)

    #columns = st.query(UserColumn).filter(UserColumn.site_id == base_link, UserColumn.type == 1,
    #                                      UserColumn.is_active == True)
    columns = UserColumn.objects.filter(site_id=base_link.site, type=1, is_active=True)
    print('columns', columns)
    for column in columns:
        print('column', column)

        #link = s.query(UserColumnLink).filter(UserColumnLink.site_id == base_link.site_id, UserColumnLink.site == True,
        #                                     UserColumnLink.link_id == base_link.id,
        #                                      UserColumnLink.column_id == column.id).all()
        link = UserColumnLink.objects.filter(site_id=base_link.site, site=True,
                                             link=base_link,
                                             column_id=column.id)
        print('update_user_link start', link)
        if not link:
            print('update_user_link yes ', link)
            now = datetime.datetime.now()
            #user_link = UserColumnLink(user_id=column.user_id, site_id=base_link.site_id, link_id=base_link.id,
            #                           site=True, show=True, column_id=column.id, is_active=True, created=now)
            user_link = UserColumnLink.objects.create(user=column.user, site_id=base_link.site,
                                                      link=base_link,
                                                      site=True, show=True,
                                                      column_id=column.id, is_active=True, created=now)
            print('success', user_link)


            #create word link
            #create_link_from_word_column(base_link, user_link, column)

            data = {"id": user_link.id, "wall_id": column.wall_id, "column_id": column.id,
                    "post": {"description": base_link.description.replace("'", "''"),
                             "id": user_link.id, "large_img": base_link.large_img,
                             "medium_img": base_link.medium_img, "small_img": base_link.small_img,
                             "tags": "", "title": base_link.title.replace("'", "''"),
                             "url": base_link.url, "thumbnail": base_link.thumbnail, "json_img": base_link.json_img,
                      "if_recontent": user_link.if_recontent, "if_favourite": user_link.if_favourite,
                      "if_recontent_id": user_link.if_recontent_id, "if_favourite_id": user_link.if_favourite_id,
                             "url_path": base_link.url}}

            cmd = json.dumps({"cmd": "INSERT", "type": "USER", "user": column.user_id, "name": "ws_new_links", "data": data})
            print('cmd', type(cmd))
            print("DATA", data)
            notify = "NOTIFY users, '"+str(cmd)+"'"
            #s.execute(text(notify))
            #UserColumnLink.objects.raw(text(notify))
            crs = connection.cursor()  # get the cursor and establish the connection.connection
            connection.connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
            crs.execute(notify)
            crs.close()
            print('notify', notify)

            update_user_link_word(base_link, user_link, column)

"""
#
# UPDATE USER LINK, CREATE LINK FOR USER COLUMN !WORD!
#
"""
@Async
def update_user_link_word(base_link, user_link, column):
    print('update_user_link_word', base_link)


    #columns_all = st.execute(text("SELECT * FROM column_to_column WHERE column_to_id = "+str(column.id)+";"))#st.query(column_to_column).filter(column_base_id==column.id)
    columns_all = ColumnToColumn.objects.filter(column_to__id=column.id)
    print('columns_all', columns_all)
    for col in columns_all:
        column = UserColumn.objects.filter(id=col.column_base_id, type=2, is_active=True)

        if column:
            column = column[0]
            settings = UserColumnSettings.objects.get(column_id=column.id)
            print('have column', settings)

            link = UserColumnLink.objects.filter(word=True, link_id=base_link.id, column_id=column.id)

            if not link:
                print('not link', link)
                text1 = base_link.title + ' ' + base_link.description
                sravni = calculation_to_word(text1, settings.word_filter, settings.word_exclude)

                if sravni:
                    print('sravni true', sravni)
                    user_link = UserColumnLink.objects.create(user=column.user, site_id=base_link.site,
                                                              link=base_link, site=False, word=True,
                                                              show=True, column_id=column.id, is_active=True,
                                                              created=datetime.datetime.now())
                    print("user_link words", user_link)

                    data = {"id": user_link.id, "wall_id": column.wall_id, "column_id": column.id,
                    "post": {"description": base_link.description.replace("'", "''"),
                             "id": user_link.id, "large_img": base_link.large_img,
                             "medium_img": base_link.medium_img, "small_img": base_link.small_img,
                             "tags": "", "title": base_link.title.replace("'", "''"),
                             "url": base_link.url,
                      "if_recontent": user_link.if_recontent, "if_favourite": user_link.if_favourite,
                      "if_recontent_id": user_link.if_recontent_id, "if_favourite_id": user_link.if_favourite_id,
                             "url_path": base_link.url}}

                    cmd = json.dumps({"cmd": "INSERT", "type": "USER", "user": column.user_id, "name": "ws_new_links", "data": data})
                    print('cmd', type(cmd))
                    print("DATA", data)
                    notify = "NOTIFY users, '"+str(cmd)+"'"
                    #s.execute(text(notify))
                    #UserColumnLink.objects.raw(text(notify))
                    crs = connection.cursor()  # get the cursor and establish the connection.connection
                    connection.connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
                    crs.execute(notify)
                    crs.close()
                    print('notify', notify)