from __future__ import unicode_literals

from django.db import models
from django.contrib.postgres.fields import JSONField

# Create your models here.
from django.db.models.signals import post_save
from djcelery.models import IntervalSchedule, PeriodicTask

"""
class CommonRoutines(Base):
    __abstract__ = True

    is_active = sa.Column(sa.Boolean, unique=False, default=False)
    updated = sa.Column(sa.DateTime)
    created = sa.Column(sa.DateTime, default=sa.func.now())
"""


class BaseModel(models.Model):
    is_active = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


"""
class User(CommonRoutines):
    __tablename__ = 'users'

    id = sa.Column(sa.Integer, primary_key=True)
    username = sa.Column(sa.String)
    first_name = sa.Column(sa.String, nullable=True)
    last_name = sa.Column(sa.String, nullable=True)
    password = sa.Column(sa.String)
    email = sa.Column(sa.String, nullable=True)

    display_name = sa.Column(sa.String(120), nullable=True)
    facebook = sa.Column(sa.String(120), nullable=True)
    github = sa.Column(sa.String(120), nullable=True)
    google = sa.Column(sa.String(120), nullable=True)
    linkedin = sa.Column(sa.String(120), nullable=True)
    twitter = sa.Column(sa.String(120), nullable=True)
    twitter_img = sa.Column(sa.String, nullable=True)
    pinterest = sa.Column(sa.String(120), nullable=True)
    instagram = sa.Column(sa.String(120), nullable=True)

    description = sa.Column(sa.String, nullable=True)
    avatar = sa.Column(sa.String, nullable=True)

    extra = sa.Column(JSONType)

    new = sa.Column(sa.Boolean, unique=False, default=True)

    last_visit = sa.Column(sa.DateTime)
    city = sa.Column(sa.String(), nullable=True)

    slug = sa.Column(sa.String, nullable=True)
    official = sa.Column(sa.Boolean, unique=False, default=False)
    official_info = sa.Column(sa.String, nullable=True)

    #followers = relationship('UserWall', secondary=usercolumn_walls,
    #                     backref=backref('user_column', lazy='dynamic'))
    followers = relationship("User",
                           secondary=user_followers,
                           primaryjoin=id == user_followers.c.user_base_id,
                           secondaryjoin=id == user_followers.c.user_to_id,
                           backref=backref('user1', lazy='dynamic')
                           )

    following = relationship("User",
                           secondary=user_following,
                           primaryjoin=id == user_following.c.user_base_id,
                           secondaryjoin=id == user_following.c.user_to_id,
                           backref=backref('user2', lazy='dynamic')
                           )

    def __init__(self, email=None, password=None, display_name=None,
                 facebook=None, github=None, google=None, linkedin=None,
                 twitter=None):
        if email:
            self.email = email.lower()
        if password:
            self.set_password(password)
        if display_name:
            self.display_name = display_name
        if facebook:
            self.facebook = facebook
        if google:
            self.google = google
        if linkedin:
            self.linkedin = linkedin
        if twitter:
            self.twitter = twitter

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def name(self):
        return self.first_name + ' ' + self.last_name

    def to_json(self):
        return dict(id=self.id, email=self.email, displayName=self.display_name,
                    facebook=self.facebook, google=self.google,
                    linkedin=self.linkedin, twitter=self.twitter, new=self.new,
                    description=self.description, first_name=self.first_name, last_name=self.last_name,
                    name=self.name(), avatar=self.avatar)

    def __repr__(self):
        return '<User: {}>'.format(self.id)
"""


class User(BaseModel):
    TYPE_TYPES = (
        (1, u'public'),
        (2, u'private')
    )

    username = models.CharField(max_length=5000, blank=True)
    first_name = models.CharField(max_length=5000, blank=True)
    last_name = models.CharField(max_length=5000, blank=True)
    password = models.CharField(max_length=5000, blank=True)
    email = models.CharField(max_length=5000, blank=True)

    type = models.IntegerField(choices=TYPE_TYPES, blank=True, null=True)

    display_name = models.CharField(max_length=5000, blank=True)
    facebook = models.CharField(max_length=5000, blank=True)
    github = models.CharField(max_length=5000, blank=True)
    google = models.CharField(max_length=5000, blank=True)
    linkedin = models.CharField(max_length=5000, blank=True)
    twitter = models.BigIntegerField(blank=True, null=True)
    twitter_img = models.CharField(max_length=5000, blank=True)
    pinterest = models.CharField(max_length=5000, blank=True)
    instagram = models.CharField(max_length=5000, blank=True)

    description = models.CharField(max_length=5000, blank=True)
    avatar = models.CharField(max_length=5000, blank=True)

    color = models.CharField(max_length=5000, blank=True)

    extra = JSONField(blank=True, null=True)

    new = models.BooleanField(default=True)

    last_visit = models.DateTimeField(blank=True, null=True)
    city = models.CharField(max_length=5000, blank=True)

    slug = models.CharField(max_length=5000, blank=True)
    official = models.BooleanField(default=False)
    official_info = models.CharField(max_length=5000, blank=True)

    class Meta:
        db_table = 'users'


"""
user_followers = sa.Table('user_followers', Base.metadata,
                          sa.Column('id', sa.Integer, primary_key=True, nullable=True),
                          sa.Column('user_base_id', sa.Integer, sa.ForeignKey('users.id'), nullable=True, index=True),
                          sa.Column('user_to_id', sa.Integer, sa.ForeignKey('users.id'), nullable=True))
"""


class UserFollowers(models.Model):
    user_base = models.ForeignKey(User, null=True, blank=True, related_name="user_followers_user_base_id",
                                     name='user_base')
    user_to = models.ForeignKey(User, null=True, blank=True, related_name="user_followers_user_to_id",
                                   name='user_to')

    class Meta:
        db_table = 'user_followers'


"""
user_following = sa.Table('user_following', Base.metadata,
                          sa.Column('id', sa.Integer, primary_key=True, nullable=True),
                          sa.Column('user_base_id', sa.Integer, sa.ForeignKey('users.id'), nullable=True, index=True),
                          sa.Column('user_to_id', sa.Integer, sa.ForeignKey('users.id'), nullable=True))
"""


class UserFollowing(models.Model):
    user_base = models.ForeignKey(User, null=True, blank=True, related_name="user_following_user_base_id",
                                     name="user_base")
    user_to = models.ForeignKey(User, null=True, blank=True, related_name="user_following_user_to_id",
                                   name="user_to")

    class Meta:
        db_table = 'user_following'


"""
class UserNotification(CommonRoutines):
    __tablename__ = 'user_notification'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=False)

    email = sa.Column(sa.String, nullable=True)
    noti_email = sa.Column(sa.Boolean, unique=False, default=False)
    noti_site = sa.Column(sa.Boolean, unique=False, default=True)
    noti_sound = sa.Column(sa.Boolean, unique=False, default=False)
    noti_digest = sa.Column(sa.Boolean, unique=False, default=False)

    interval_digest = sa.Column(sa.String, nullable=True)
"""


class UserNotification(BaseModel):
    user_id = models.ForeignKey(User, null=True, blank=True)

    email = models.CharField(max_length=5000, blank=True)
    noti_email = models.CharField(max_length=5000, blank=True)
    noti_site = models.CharField(max_length=5000, blank=True)
    noti_sound = models.CharField(max_length=5000, blank=True)
    noti_digest = models.CharField(max_length=5000, blank=True)

    interval_digest = models.CharField(max_length=5000, blank=True)

    class Meta:
        db_table = 'user_notification'


"""
class BaseSite(CommonRoutines):
    __tablename__ = 'base_site'

    id = sa.Column(sa.Integer, primary_key=True)

    name = sa.Column(sa.String, nullable=True)
    description = sa.Column(sa.String, nullable=True)
    url = sa.Column(sa.String, nullable=True)
    short_url = sa.Column(sa.String, nullable=True)
    rss = sa.Column(sa.String, nullable=True)  # url rss
    image = sa.Column(sa.String, nullable=True)
    favicon = sa.Column(sa.String, nullable=True)
    color = sa.Column(sa.String, nullable=True)

    parent = sa.Column(sa.Boolean, unique=False, default=True)
    parentid = sa.Column(sa.Integer, nullable=True)

    extra = sa.Column(JSONType)  # extra information site

    custom = sa.Column(sa.Boolean, default=True)
"""


class BaseSite(BaseModel):
    name = models.CharField(max_length=5000, blank=True)
    description = models.CharField(max_length=5000, blank=True)
    url = models.CharField(max_length=5000, blank=True)
    short_url = models.CharField(max_length=5000, blank=True)
    rss = models.CharField(max_length=5000, blank=True)
    image = models.CharField(max_length=5000, blank=True)
    favicon = models.CharField(max_length=5000, blank=True)
    color = models.CharField(max_length=5000, blank=True)

    parent = models.BooleanField(blank=True)
    parentid = models.IntegerField(null=True, blank=True)

    extra = JSONField(blank=True, null=True)
    custom = models.BooleanField(blank=True)

    class Meta:
        db_table = 'base_site'


"""
class BaseLink(CommonRoutines):
    __tablename__ = 'base_link'

    id = sa.Column(sa.Integer, primary_key=True)

    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=False)

    title = sa.Column(sa.String, nullable=True)
    description = sa.Column(sa.String, nullable=True)
    keywords = sa.Column(sa.String, nullable=True)

    url = sa.Column(sa.String, nullable=True)
    full_url = sa.Column(sa.String, nullable=True)  # url rss

    small_img = sa.Column(sa.String, nullable=True)
    medium_img = sa.Column(sa.String, nullable=True)
    large_img = sa.Column(sa.String, nullable=True)
    site_img = sa.Column(sa.String, nullable=True)
    site_img_url = sa.Column(sa.String, nullable=True)

    tags = sa.Column(sa.String, nullable=True)

    show = sa.Column(sa.Boolean, unique=False, default=True)

    extra = sa.Column(JSONType)  # extra information link
"""


class BaseLink(BaseModel):
    site = models.ForeignKey(BaseSite, blank=True, null=True, name='site')

    title = models.CharField(max_length=5000, blank=True)
    description = models.CharField(max_length=5000, blank=True)
    keywords = models.CharField(max_length=5000, blank=True)

    url = models.CharField(max_length=15000, blank=True)
    full_url = models.CharField(max_length=15000, blank=True)

    thumbnail = models.TextField(blank=True, null=True)
    extra_img = models.TextField(blank=True, null=True)
    small_img = models.TextField(blank=True)
    medium_img = models.TextField(blank=True)
    large_img = models.TextField(blank=True)
    site_img = models.TextField(blank=True)
    site_img_url = models.TextField(blank=True)
    json_img = JSONField(null=True)

    html = models.TextField(blank=True)

    tags = models.CharField(max_length=5000, blank=True)

    show = models.BooleanField(blank=True)

    extra = JSONField(blank=True, null=True)

    class Meta:
        db_table = 'base_link'


"""
class UserWall(CommonRoutines):
    __tablename__ = 'user_wall'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=False)

    slug = sa.Column(sa.String, nullable=True)
    name = sa.Column(sa.String, nullable=True)
    color = sa.Column(sa.String, nullable=True)
    description = sa.Column(sa.String, nullable=True)

    follow_user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    follow = sa.Column(sa.Boolean, unique=False, default=False)

    icon = sa.Column(sa.String, nullable=True)

"""


class UserWall(BaseModel):
    user = models.ForeignKey(User, null=True, blank=True, related_name='user_wall_user_id', name='user')

    slug = models.CharField(max_length=5000, blank=True)
    name = models.CharField(max_length=5000, blank=True)
    color = models.CharField(max_length=5000, blank=True)
    description = models.CharField(max_length=5000, blank=True)

    follow_user = models.ForeignKey(User, null=True, blank=True, related_name='user_wall_follow_user_id',
                                    name='follow_user')
    follow = models.BooleanField(default=False, blank=True)

    icon = models.CharField(max_length=5000, blank=True)

    sort = models.IntegerField(blank=True, null=True)
    category_id = models.IntegerField(blank=True, null=True)

    thumbnail = models.CharField(max_length=5000, blank=True)
    medium_img = models.CharField(max_length=5000, blank=True)

    is_top = models.BooleanField(default=False, blank=True)
    is_trends = models.BooleanField(default=False, blank=True)
    is_collections = models.BooleanField(default=False, blank=True)
    is_for_you = models.BooleanField(default=False, blank=True)

    class Meta:
        db_table = 'user_wall'


"""
class UserColumnLink(CommonRoutines):
    __tablename__ = 'user_column_link'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=True)
    link_id = sa.Column(sa.Integer, sa.ForeignKey('base_link.id'), nullable=True)
    column_id = sa.Column(sa.Integer, nullable=True)

    recontent = sa.Column(sa.Boolean, unique=False, default=False)
    related = sa.Column(sa.Boolean, unique=False, default=False)
    word = sa.Column(sa.Boolean, unique=False, default=False)
    site = sa.Column(sa.Boolean, unique=False, default=True)
    show = sa.Column(sa.Boolean, unique=False, default=True)

    counter = sa.Column(sa.Integer, nullable=True)# count perehodov

    favourite = sa.Column(sa.Boolean, unique=False, default=False)
    fav_add = sa.Column(sa.DateTime, nullable=True)

    related_children = sa.Column(sa.Integer, sa.ForeignKey('user_column_link.id'), nullable=True)
"""


class UserColumnLink(BaseModel):
    user = models.ForeignKey(User, null=True, blank=True, related_name='user_column_link_user_id', name='user')
    site_id = models.ForeignKey(BaseSite, blank=True, null=True, related_name='user_column_link_site', name='site_id',
                                db_column='site_id')
    link = models.ForeignKey(BaseLink, blank=True, null=True, related_name='user_column_link_link_id', name='link')
    column_id = models.IntegerField(blank=False, null=True)

    recontent = models.BooleanField(default=False, blank=True)
    related = models.BooleanField(default=False, blank=True)
    word = models.BooleanField(default=False, blank=True)
    site = models.BooleanField(blank=True)
    show = models.BooleanField(blank=True)

    if_recontent = models.BooleanField(blank=True)
    if_favourite = models.BooleanField(blank=True)
    if_recontent_id = models.IntegerField(blank=False, null=True)
    if_favourite_id = models.IntegerField(blank=False, null=True)

    counter = models.IntegerField(blank=True, null=True)

    favourite = models.BooleanField(default=False, blank=True)
    fav_add = models.DateTimeField(blank=True, null=True)

    related_children = models.ForeignKey("self", blank=True, null=True, name='related_children',
                                         db_column='related_children')

    class Meta:
        db_table = 'user_column_link'


"""
class CategoryModel(CommonRoutines):
    TYPE_TYPES = [
        (1, u'base'),
        (2, u'for user'),
    ]
    __tablename__ = 'category_model'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=True)

    slug = sa.Column(sa.String, nullable=True)
    short_name = sa.Column(sa.String, nullable=True)
    name = sa.Column(sa.String, nullable=True)
    description = sa.Column(sa.String, nullable=True)
    type = sa.Column(sa.Integer, nullable=True, default=1)

    word_filter = sa.Column(sa.String, nullable=True)
    word_exclude = sa.Column(sa.String, nullable=True)
    tags = sa.Column(sa.String, nullable=True)

    extra = sa.Column(sa.String, nullable=True)  # to json

    color = sa.Column(sa.String, nullable=True)
    icon = sa.Column(sa.String, nullable=True)

    is_admin = sa.Column(sa.Boolean, unique=False, default=False)
    is_show = sa.Column(sa.Boolean, unique=False, default=False)

    is_top = sa.Column(sa.Boolean, unique=False, default=False)
    is_trends = sa.Column(sa.Boolean, unique=False, default=False)
    is_collections = sa.Column(sa.Boolean, unique=False, default=False)
    is_for_you = sa.Column(sa.Boolean, unique=False, default=False)

"""


class CategoryModel(BaseModel):
    TYPE_TYPES = (
        (1, u'base'),
        (2, u'for user'),
    )

    user_id = models.ForeignKey(User, null=True, blank=True, related_name='category_model_user_id', name='user_id',
                                db_column='user_id')
    site_id = models.ForeignKey(BaseSite, blank=True, null=True, related_name='category_model_site_id', name='site_id',
                                db_column='site_id')

    slug = models.CharField(max_length=5000, blank=True)
    short_name = models.CharField(max_length=5000, blank=True)
    name = models.CharField(max_length=5000, blank=True)
    description = models.CharField(max_length=5000, blank=True)
    type = models.IntegerField(choices=TYPE_TYPES, blank=True, null=True)

    word_filter = models.CharField(max_length=5000, blank=True)
    word_exclude = models.CharField(max_length=5000, blank=True)
    tags = models.CharField(max_length=5000, blank=True)

    extra = models.TextField(blank=True)

    color = models.CharField(max_length=5000, blank=True)
    icon = models.CharField(max_length=5000, blank=True)

    is_admin = models.BooleanField(default=False)
    is_show = models.BooleanField(default=False)

    is_top = models.BooleanField(default=False)
    is_trends = models.BooleanField(default=False)
    is_collections = models.BooleanField(default=False)
    is_for_you = models.BooleanField(default=False)

    class Meta:
        db_table = 'category_model'


"""
class UserColumn(CommonRoutines):
    TYPE_TYPES = [
        (1, u'Site'),
        (2, u'Word'),
        (3, u'recontent'),
        (4, u'related')
    ]
    __tablename__ = 'user_column'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    wall_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True)
    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=True)

    column_to_rel_column = sa.Column(sa.Integer, nullable=True, default=None)

    related_link_id = sa.Column(sa.Integer, sa.ForeignKey('user_column_link.id'), nullable=True)

    walls = relationship('UserWall', secondary=usercolumn_walls,
                         backref=backref('user_column', lazy='dynamic'))

    columns = relationship("UserColumn",
                           secondary=column_to_column,
                           primaryjoin=id == column_to_column.c.column_base_id,
                           secondaryjoin=id == column_to_column.c.column_to_id,
                           backref=backref('columns1', lazy='dynamic')
                           )

    slug = sa.Column(sa.String, nullable=True)
    name = sa.Column(sa.String, nullable=True)
    description = sa.Column(sa.String, nullable=True)
    type = sa.Column(sa.Integer, nullable=True, default=1)

    follow_user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    follow = sa.Column(sa.Boolean, unique=False, default=False)

    color = sa.Column(sa.String, nullable=True)
    icon = sa.Column(sa.String, nullable=True)
"""


class UserColumn(BaseModel):
    TYPE_TYPES = (
        (1, u'Site'),
        (2, u'Word'),
        (3, u'recontent'),
        (4, u'related'),
        (5, u'top'),
        (6, u'trends'),
        (7, u'recommend for you')
    )

    user = models.ForeignKey(User, null=True, blank=True, related_name='user_column_user_id', name='user')
    site_id = models.ForeignKey(BaseSite, blank=True, null=True, related_name='user_column_site_id', name='site_id',
                                db_column='site_id')
    wall = models.ForeignKey(UserWall, blank=True, null=True, related_name='user_column_wall_id', name='wall')

    column_to_rel_column = models.IntegerField(blank=True, null=True)

    related_link = models.ForeignKey("self", blank=True, null=True, name='related_link')

    slug = models.CharField(max_length=5000, blank=True)
    name = models.CharField(max_length=5000, blank=True)
    description = models.CharField(max_length=5000, blank=True)
    type = models.IntegerField(choices=TYPE_TYPES, blank=True, null=True)

    follow_user = models.ForeignKey(User, null=True, blank=True, related_name='user_column_follow_user_id',
                                    name='follow_user')
    follow = models.BooleanField(default=False, blank=True)

    color = models.CharField(max_length=5000, blank=True)
    icon = models.CharField(max_length=5000, blank=True)

    class Meta:
        db_table = 'user_column'


"""
usercolumn_walls = sa.Table('usercolumn_walls', Base.metadata,
                          sa.Column('id', sa.Integer, primary_key=True, nullable=True),
                          sa.Column('user_wall_id', sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True, index=True),
                          sa.Column('usercolumn_wall_id', sa.Integer, sa.ForeignKey('user_column.id'), nullable=True))
"""


class UserColumnWalls(models.Model):
    user_wall = models.ForeignKey(UserWall, null=True, blank=True, related_name="usercolumn_walls_user_wall_id")
    usercolumn_wall = models.ForeignKey(UserColumn, null=True, blank=True,
                                        related_name="usercolumn_walls_usercolumn_wall_id")

    class Meta:
        db_table = 'usercolumn_walls'


"""
userwall_sites = sa.Table('userwall_sites', Base.metadata,
                            sa.Column('id', sa.Integer, primary_key=True, nullable=True),
                            sa.Column('userwall_wall_id', sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True,
                                      index=True),
                            sa.Column('userwall_site_id', sa.Integer, sa.ForeignKey('base_site.id'), nullable=True))
"""

class UserWallSites(models.Model):
    userwall_wall = models.ForeignKey(UserWall, null=True, blank=True, related_name="userwall_sites_user_wall_id",
                                      name="userwall_wall")
    userwall_site = models.ForeignKey(BaseSite, null=True, blank=True,
                                        related_name="userwall_sites_userwall_site_id", name="userwall_site")

    class Meta:
        db_table = 'userwall_sites'

"""
column_to_column = sa.Table('column_to_column', Base.metadata,
                          sa.Column('id', sa.Integer, primary_key=True, nullable=True),
                          sa.Column('column_base_id', sa.Integer, sa.ForeignKey('user_column.id'), nullable=True, index=True),
                          sa.Column('column_to_id', sa.Integer, sa.ForeignKey('user_column.id'), nullable=True))
"""


class ColumnToColumn(models.Model):
    column_base = models.ForeignKey(UserColumn, null=True, blank=True, related_name="column_to_column_column_base_id")
    column_to = models.ForeignKey(UserColumn, null=True, blank=True, related_name="column_to_column_column_to_id")

    class Meta:
        db_table = 'column_to_column'


"""
class CollectionsModel(CommonRoutines):
    TYPE_TYPES = [
        (1, u'base'),
        (2, u'for user'),
    ]
    __tablename__ = 'collections_model'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    wall_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True)
    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=True)
    column_id = sa.Column(sa.Integer, sa.ForeignKey('user_column.id'), nullable=True)

    slug = sa.Column(sa.String, nullable=True)
    short_name = sa.Column(sa.String, nullable=True)
    name = sa.Column(sa.String, nullable=True)
    description = sa.Column(sa.String, nullable=True)
    type = sa.Column(sa.Integer, nullable=True, default=1)

    extra = sa.Column(sa.String, nullable=True)  # to json

    color = sa.Column(sa.String, nullable=True)
    icon = sa.Column(sa.String, nullable=True)
"""


class CollectionsModel(BaseModel):
    TYPE_TYPES = (
        (1, u'base'),
        (2, u'for user'),
    )

    user_id = models.ForeignKey(User, null=True, blank=True, related_name='collections_model_user_id', name='user_id',
                                db_column='user_id')
    wall_id = models.ForeignKey(UserWall, blank=True, null=True, related_name='collections_model_id', name='wall_id',
                                db_column='wall_id')
    site_id = models.ForeignKey(BaseSite, blank=True, null=True, related_name='collections_model_site_id',
                                name='site_id', db_column='site_id')
    column_id = models.ForeignKey(UserColumn, blank=True, null=True, related_name='collections_model_column_id',
                                  name='column_id', db_column='column_id')

    slug = models.CharField(max_length=5000, blank=True)
    short_name = models.CharField(max_length=5000, blank=True)
    name = models.CharField(max_length=5000, blank=True)
    description = models.CharField(max_length=5000, blank=True)
    type = models.IntegerField(choices=TYPE_TYPES, blank=True, null=True)

    extra = models.TextField(blank=True)

    color = models.CharField(max_length=5000, blank=True)
    icon = models.CharField(max_length=5000, blank=True)

    class Meta:
        db_table = 'collections_model'


"""
class CategoryToModel(CommonRoutines):
    __tablename__ = 'category_to_model'

    id = sa.Column(sa.Integer, primary_key=True)

    category_model_id = sa.Column(sa.Integer, sa.ForeignKey('category_model.id'), nullable=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    column_id = sa.Column(sa.Integer, sa.ForeignKey('user_column.id'), nullable=True)
    wall_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True)
    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=True)
    collection_id = sa.Column(sa.Integer, sa.ForeignKey('collections_model.id'), nullable=True)

    is_admin = sa.Column(sa.Boolean, unique=False, default=False)
    is_show = sa.Column(sa.Boolean, unique=False, default=False)

    is_top = sa.Column(sa.Boolean, unique=False, default=False)
    is_trends = sa.Column(sa.Boolean, unique=False, default=False)
    is_collections = sa.Column(sa.Boolean, unique=False, default=False)
    is_for_you = sa.Column(sa.Boolean, unique=False, default=False)

    extra = sa.Column(sa.String, nullable=True)  # to json
"""


class CategoryToModel(BaseModel):
    TYPE_TYPES = (
        (1, u'base'),
        (2, u'for user'),
    )

    category_model_id = models.ForeignKey(CategoryModel, null=True, blank=True,
                                          related_name='category_to_model_category_model_id', name='category_model_id',
                                          db_column='category_model_id')
    user_id = models.ForeignKey(User, null=True, blank=True, related_name='category_to_model_user_id', name='user_id',
                                db_column='user_id')
    column_id = models.ForeignKey(UserColumn, blank=True, null=True, related_name='category_to_model_column_id',
                                  name='column_id', db_column='column_id')
    wall_id = models.ForeignKey(UserWall, blank=True, null=True, related_name='category_to_model_id', name='wall_id',
                                db_column='wall_id')
    site_id = models.ForeignKey(BaseSite, blank=True, null=True, related_name='category_to_model_site_id',
                                name='site_id', db_column='site_id')
    collection_id = models.ForeignKey(CollectionsModel, null=True, blank=True,
                                      related_name='category_to_model_collection_id',
                                      name='collection_id',
                                      db_column='collection_id')

    is_admin = models.BooleanField(default=False)
    is_show = models.BooleanField(default=False)

    is_top = models.BooleanField(default=False)
    is_trends = models.BooleanField(default=False)
    is_collections = models.BooleanField(default=False)
    is_for_you = models.BooleanField(default=False)

    extra = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'category_to_model'


"""
class UserColumnSettings(CommonRoutines):
    SIZE_TYPES = [
        (200, u'size200'),
        (300, u'size300'),
        (400, u'size400'),
        (500, u'size500'),
        (600, u'size600'),
        (1000, u'size_other'),
    ]
    TYPE_TYPES = [
        (1, u'public'),
        (2, u'private'),
    ]
    __tablename__ = 'user_column_settings'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    column_id = sa.Column(sa.Integer, sa.ForeignKey('user_column.id'), nullable=True)

    word_filter = sa.Column(sa.String, nullable=True)
    word_exclude = sa.Column(sa.String, nullable=True)

    sort = sa.Column(sa.Integer, nullable=True)

    size = sa.Column(ChoiceType(SIZE_TYPES, impl=sa.Integer()), nullable=True, default=300)
    type = sa.Column(ChoiceType(TYPE_TYPES, impl=sa.Integer()), nullable=True, default=1)

    def as_dict(self):
       return {'word_filter': self.get_word_filter(), 'word_exclude': self.get_word_exclude(), 'sort': self.sort,
               'size': self.size.code, 'type': self.type.code}

    def get_word_exclude(self):
        try:
            return self.word_exclude.split(', ')
        except:
            return []

    def get_word_filter(self):
        try:
            return self.word_filter.split(', ')
        except:
            return []
"""


class UserColumnSettings(BaseModel):
    SIZE_TYPES = (
        (200, u'size200'),
        (300, u'size300'),
        (400, u'size400'),
        (500, u'size500'),
        (600, u'size600'),
        (1000, u'size_other')
    )
    TYPE_TYPES = (
        (1, u'public'),
        (2, u'private')
    )

    user = models.ForeignKey(User, null=True, blank=True, related_name='user_column_settings_user_id', name='user')
    column = models.ForeignKey(UserColumn, blank=True, null=True, related_name='user_column_settings_column_id',
                               name='column')

    word_filter = models.CharField(max_length=5000, blank=True)
    word_exclude = models.CharField(max_length=5000, blank=True)

    sort = models.IntegerField(blank=True, null=True)
    realtime = models.BooleanField(default=False, blank=True)
    size = models.IntegerField(choices=SIZE_TYPES, blank=True, null=True)
    type = models.IntegerField(choices=TYPE_TYPES, blank=True, null=True)

    class Meta:
        db_table = 'user_column_settings'


"""
class UserColumnNotification(CommonRoutines):
    __tablename__ = 'user_column_notification'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    column_id = sa.Column(sa.Integer, sa.ForeignKey('user_column.id'), nullable=True)

    email = sa.Column(sa.String, nullable=True)
    noti_email = sa.Column(sa.Boolean, unique=False, default=False)
    noti_site = sa.Column(sa.Boolean, unique=False, default=True)
    noti_sound = sa.Column(sa.Boolean, unique=False, default=False)
    noti_digest = sa.Column(sa.Boolean, unique=False, default=False)

    interval_digest = sa.Column(sa.String, nullable=True) # day, week, month
"""


class UserColumnNotification(BaseModel):
    user = models.ForeignKey(User, null=True, blank=True, related_name='user_column_notification_user_id', name='user')
    column = models.ForeignKey(UserColumn, blank=True, null=True, related_name='user_column_notification_column_id',
                               name='column')

    email = models.CharField(max_length=5000, blank=True)
    noti_email = models.CharField(max_length=5000, blank=True)
    noti_site = models.CharField(max_length=5000, blank=True)
    noti_sound = models.CharField(max_length=5000, blank=True)
    noti_digest = models.CharField(max_length=5000, blank=True)

    interval_digest = models.CharField(max_length=5000, blank=True)

    class Meta:
        db_table = 'user_column_notification'


"""
class UserWallNotification(CommonRoutines):
    __tablename__ = 'user_wall_notification'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    wall_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True)

    email = sa.Column(sa.String, nullable=True)
    noti_email = sa.Column(sa.Boolean, unique=False, default=False)
    noti_site = sa.Column(sa.Boolean, unique=False, default=True)
    noti_sound = sa.Column(sa.Boolean, unique=False, default=False)
    noti_digest = sa.Column(sa.Boolean, unique=False, default=False)

    interval_digest = sa.Column(sa.String, nullable=True) # day, week, month
"""


class UserWallNotification(BaseModel):
    user = models.ForeignKey(User, null=True, blank=True, related_name='user_wall_notification_user_id', name='user')
    wall = models.ForeignKey(UserWall, blank=True, null=True, related_name='user_wall_notification_wall_id',
                             name='wall')

    email = models.CharField(max_length=5000, blank=True)
    noti_email = models.CharField(max_length=5000, blank=True)
    noti_site = models.CharField(max_length=5000, blank=True)
    noti_sound = models.CharField(max_length=5000, blank=True)
    noti_digest = models.CharField(max_length=5000, blank=True)

    interval_digest = models.CharField(max_length=5000, blank=True)

    class Meta:
        db_table = 'user_wall_notification'


"""
class UserColumnWallSort(CommonRoutines):
    __tablename__ = 'user_column_wall_sort'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    wall_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True)
    column_id = sa.Column(sa.Integer, sa.ForeignKey('user_column.id'), nullable=True)

    sort = sa.Column(sa.Integer, nullable=True)
"""


class UserColumnWallSort(BaseModel):
    user = models.ForeignKey(User, null=True, blank=True, related_name='user_column_wall_sort_user_id', name='user')
    wall = models.ForeignKey(UserWall, blank=True, null=True, related_name='user_column_wall_sort_wall_id', name='wall')
    column = models.ForeignKey(UserColumn, blank=True, null=True, related_name='user_column_wall_sort_column_id',
                               name='column')

    sort = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'user_column_wall_sort'


"""
class UserToNotification(CommonRoutines):
    TYPE_TYPES = [
        (1, u'addcolumn'),
        (2, u'follow'),
        (3, u'unfollow'),
        (4, u'addwall'),
        (5, u'other'),
    ]
    __tablename__ = 'user_to_notification'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)

    user_to_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    column_to_id = sa.Column(sa.Integer, sa.ForeignKey('user_column.id'), nullable=True)

    type = sa.Column(ChoiceType(TYPE_TYPES, impl=sa.Integer()), nullable=True)
    action = sa.Column(sa.String, nullable=True)

    msg = sa.Column(sa.String, nullable=True)
"""


class UserToNotification(BaseModel):
    TYPE_TYPES = (
        (1, u'addcolumn'),
        (2, u'follow'),
        (3, u'unfollow'),
        (4, u'addwall'),
        (5, u'other'),
    )

    user_id = models.ForeignKey(User, null=True, blank=True, related_name='user_to_notification_user_id',
                                name='user')

    column_to_id = models.ForeignKey(UserColumn, blank=True, null=True,
                                     related_name='user_to_notification_column_to_id', name='column_to')
    user_to_id = models.ForeignKey(User, null=True, blank=True,
                                   related_name='user_to_notification_user_to_id', name='user_to')

    type = models.IntegerField(choices=TYPE_TYPES, blank=True, null=True)
    action = models.CharField(max_length=5000, blank=True)
    msg = models.CharField(max_length=5000, blank=True)

    class Meta:
        db_table = 'user_to_notification'


def add_after_base_site_to_celery(sender, **kwargs):
    obj = kwargs["instance"]
    if kwargs["created"]:

        interval = IntervalSchedule.objects.get(id=3)

        name = 'create_links ' + str(obj.id)
        ar = '['+str(obj.id)+']'

        PeriodicTask.objects.create(name=name, task='core.tasks.create_links', interval=interval,
                                    args=ar, enabled=obj.is_active)
    else:
        name = 'create_links ' + str(obj.id)

        period = PeriodicTask.objects.get(name=name)
        period.enabled = obj.is_active
        period.save()


post_save.connect(add_after_base_site_to_celery, sender=BaseSite)
