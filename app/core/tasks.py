from celery.task import task

from core.engine import launch


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def just_print():
    print "Print from celery task"

@task
def only():
    print('only')
    print "Print from celery task"

@task
def create_links(id):
    print('create_links222222222222222', id)
    launch(id)
    print('create_links', id)
    print "create_links create_links"
