from django.contrib import admin

# Register your models here.
from core.models import *

class BaseSiteAdmin(admin.ModelAdmin):
    fields = ('name', 'url', 'short_url', 'rss', 'is_active', 'favicon', 'color')
    search_fields = ['id', 'name', 'url', 'short_url']
    list_display = ('id', 'name', 'url', 'short_url', 'rss', 'is_active', 'favicon', 'color')


admin.site.register(User)
admin.site.register(UserFollowers)
admin.site.register(UserFollowing)
admin.site.register(UserNotification)
admin.site.register(BaseSite, BaseSiteAdmin)
admin.site.register(BaseLink)
admin.site.register(UserWall)
admin.site.register(UserColumnLink)
admin.site.register(UserColumn)
admin.site.register(UserColumnWalls)
admin.site.register(ColumnToColumn)
admin.site.register(UserColumnSettings)
admin.site.register(UserColumnNotification)
admin.site.register(UserWallNotification)
admin.site.register(UserColumnWallSort)
admin.site.register(UserToNotification)
admin.site.register(CategoryModel)
admin.site.register(CollectionsModel)
admin.site.register(CategoryToModel)
admin.site.register(UserWallSites)